/*
 * main.cpp
 *
 *  Created on: 12 de mar. de 2017
 *      Author: Blinsky
 */

#include <fstream>
#include <iostream>
#include <math.h>
using namespace std;

void *__gxx_personality_v0;

// ----- PROTOTYPES
double mapToReal(int x, int imageWidth, double minR, double maxR);
double mapToImaginary(int y, int imageHeight, double minI, double maxI);
int findMandelbrot(double cr, double ci, int max_it);

// ----- MAIN
int main(){
	//Get input file...
	ifstream fin("input.txt");
	int imgWidth, imgHeight, maxN;
	double minR,maxR,minI,maxI;
	if(!fin){
		cout << "Could not open file 'input.txt'" << endl;
		cin.ignore();
		return 0;
	}
	fin >> imgWidth >> imgHeight >> maxN;
	fin >> minR >> maxR >> minI >> maxI;
	fin.close();
	//Open output file, write PPM
	ofstream fout("output_img.ppm");
	fout << "P3" << endl; //PPM header declaration
	fout << imgWidth << " " << imgHeight << endl; //Dimensions
	fout << "256" << endl; //Max value of RGB pixel


	//For every pixel...
	for(int y=0; y < imgHeight; y++){ //Rows
		for(int x=0; x < imgWidth; x++){
			//... Find the real and imaginary value of c,
			//     corresponding to x, y pixel
			double cr = mapToReal(x, imgWidth, minR, maxR);
			double ci = mapToImaginary(y, imgHeight, minR, maxR);

			//... Find number of iterations in Mandelbrot formula
			//     using c
			int n = findMandelbrot(cr, ci, maxN);

			//... Map resulting number to RGB value
			int r = ( (int) pow(n*cos(n), 0.5)	% 256);
			int g = ( (int) pow(n*cos(n), 1.0) 			% 256);
			int b = ( (int) pow(n*cos(n), 1.5) 					% 256);

			//... Output value to image
			fout << r << " " << g << " " << b << " ";
		}
		fout << endl;
	}
	fout.close();
	cout << "Done!" << endl;
	return 0;

}

// ----- DEFINITIONS

double mapToReal(int x, int imageWidth, double minR, double maxR){
	double range = maxR-minR;
	return x * (range / imageWidth) + minR;
};

double mapToImaginary(int y, int imageHeight, double minI, double maxI){
	double range = maxI-minI;
	return y * (range / imageHeight) + minI;
}

int findMandelbrot(double cr, double ci, int max_it){
	int i = 0;
	double zr = 0.0, zi = 0.0;
	while(i < max_it && (zr*zr + zi*zi < 4.0) ){
		double temp = zr * zr - zi * zi + cr;
		zi = 2.0 * zr * zi + ci;
		zr = temp;
		i++;
	}
	return i;
}
